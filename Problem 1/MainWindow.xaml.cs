﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Problem_1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
            int answer = 0;

            for(int count = 1; count < 1000; count++)
            {
                if (count % 3 == 0)
                {
                    answer = answer + count;
                }
                else if (count % 5 == 0)
                {
                    answer = answer + count;
                }
            }

            listBoxAnswer.Items.Add("Answer = " + answer);

        }
    }
}
